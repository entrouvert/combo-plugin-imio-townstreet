# -*- coding: utf-8 -*-

from combo.data.models import Page

from combo_plugin_imio_townstreet.models import TownstreetButton


def test_button(app):
    page = Page(title='One', slug='index')
    page.save()
    button = TownstreetButton(page=page, order=0, placeholder='content')
    button.save()
    resp = app.get('/', status=200)
    assert 'Signaler un problème sur l’espace public' in resp
