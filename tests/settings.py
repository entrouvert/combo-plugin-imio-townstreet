import os

INSTALLED_APPS += ('combo_plugin_imio_townstreet',)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'TEST': {
            'NAME': 'combo-plugin-imio-townstreet-test-%s'
            % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:29],
        },
    }
}
