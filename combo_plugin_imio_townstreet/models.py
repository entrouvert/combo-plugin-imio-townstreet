# combo-plugin-imio-townstreet - Combo iMio town street plugin
# Copyright (C) 2021  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.utils.translation import gettext_lazy as _

from combo.data.models import CellBase
from combo.data.library import register_cell_class


@register_cell_class
class TownstreetButton(CellBase):
    template_name = 'imio_townstreet/button.html'
    default_template_name = 'imio_townstreet/button.html'

    class Meta:
        unique_together = [('page', 'uuid')]
        verbose_name = _('Townstreet Button')

    class Media:
        css = {
            'all': ('imio_townstreet/style.css',),
        }

    def get_cell_extra_context(self, context):
        extra_context = super().get_cell_extra_context(context)
        extra_context['url'] = getattr(settings, 'TOWNSTREET_BUTTON_URL', '#')
        return extra_context
